up:
	@docker-compose up
build:
	@docker-compose build
down:
	@docker-compose down
vue-bash:
	@docker exec -it vue /bin/bash
express-bash:
	@docker exec -it express /bin/bash
