FROM node:17

USER node

ENV NPM_CONFIG_PREFIX=/home/node/.npm-global

ENV PATH=$PATH:/home/node/.npm-global/bin

RUN npm install -g nodemon

COPY --chown=node ./express/ /usr/src/app

WORKDIR /usr/src/app

RUN npm install && npm cache clean --force
