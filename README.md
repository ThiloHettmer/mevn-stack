# MEVN Stack

This project includes the famous MEVN stack:

- Mongo
- ExpressJS
- Vue 3 (Vite with Typescript)
- Node

## Prerequisites

- git
- make
- docker
- docker-compose

## Build

1) Run `make build` to build all containers

## Start

1) Run `make up` to start the stack

## Testing Frameworks

Coming soon

## Troubleshooting

Right now the latest vite version does not work with hmr. This means this project uses vite@2.8.6.

## Author

This project was created by Thilo Hettmer. 

Gitlab: https://gitlab.com/ThiloHettmer

## Licence

MIT License

Copyright (c) 2022 Thilo Hettmer

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
