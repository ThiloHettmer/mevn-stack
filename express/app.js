const express = require('express');
const path = require('path');

const exampleRoute = require('./routes/example');

const app = express();

app.use(express.urlencoded({ extended: false }));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', exampleRoute);

module.exports = app;
